package com.projects.movielist.features.movielist;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.projects.movielist.GlobalApp;
import com.projects.movielist.connections.databases.InterfaceMovieListDatabase;
import com.projects.movielist.connections.networks.ConverterUrlRequest;
import com.projects.movielist.connections.networks.PathUrl;
import com.projects.movielist.connections.request.GetMovieListFromDatabaseRequest;
import com.projects.movielist.connections.request.GetMovieListRequest;
import com.projects.movielist.connections.response.DatabaseResponseErrorListener;
import com.projects.movielist.connections.response.DatabaseResponseSuccessListener;
import com.projects.movielist.connections.response.GetMovieListResponse;
import com.projects.movielist.helpers.GlobalVariable;
import com.projects.movielist.models.MovieModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter

public class MovieListViewModel extends AndroidViewModel {

    protected InterfaceMovieListDatabase database;
    private final RequestQueue requestQueue;
    private final MutableLiveData<List<MovieModel>> movieLiveData = new MutableLiveData<>();
    private final MutableLiveData<Integer> totalPageLiveData =  new MutableLiveData<>();
    private final MutableLiveData<Integer> totalPageDatabaseLiveData = new MutableLiveData<>();

    public MovieListViewModel(@NonNull Application application) {
        super(application);

        requestQueue = Volley.newRequestQueue(application);
        database = GlobalApp.getNewInstance().getMovieListDatabase(application);
    }

    public void getMovieList(GetMovieListRequest request){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(GlobalVariable.KEY_URL_PAGE, request.getPage());
            jsonObject.put(GlobalVariable.KEY_URL_API_KEY, request.getApiKey());
        } catch (Exception e) {
            e.printStackTrace();
        }

        ConverterUrlRequest getRequest = new ConverterUrlRequest(Request.Method.GET, PathUrl.getListId1(), jsonObject,
                response -> {
                    GetMovieListResponse getMovieListResponse = new GetMovieListResponse(response);
                    movieLiveData.postValue(getMovieListResponse.getMovieModelList());
                    totalPageLiveData.postValue(getMovieListResponse.getTotalPage());
                    Log.v("API response", response.toString());
                },
                error -> {
                    Log.v("API Error", error.toString());
                } );
        requestQueue.add(getRequest);
    }

    public void insertMovieListToDatabase(List<MovieModel> models){
        for (MovieModel model : models){
            database.insertStore(
                    model,
                    new DatabaseResponseSuccessListener<Long>() {
                        @Override
                        public void onSuccessResponse(Long results) {

                        }
                    },
                    new DatabaseResponseErrorListener() {
                        @Override
                        public void onSuccessResponse(Object results) {

                        }
                    }
            );
        }
    }

    public void getMovieListFromDatabase(GetMovieListFromDatabaseRequest request){
        database.getMovieList(
                request,
                new DatabaseResponseSuccessListener<List<MovieModel>>() {
                    @Override
                    public void onSuccessResponse(List<MovieModel> results) {
                        movieLiveData.postValue(results);
                    }
                },
                new DatabaseResponseErrorListener() {
                    @Override
                    public void onSuccessResponse(Object results) {

                    }
                }
        );
        database.getTotalMovie(GlobalVariable.PAGE_SIZE,
                new DatabaseResponseSuccessListener<Integer>() {
                    @Override
                    public void onSuccessResponse(Integer results) {
                        totalPageDatabaseLiveData.postValue(results);
                    }
                },
                new DatabaseResponseErrorListener() {
                    @Override
                    public void onSuccessResponse(Object results) {

                    }
                });
    }
}
