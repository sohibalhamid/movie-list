package com.projects.movielist.connections.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public abstract class DatabaseResponseSuccessListener<R> implements Runnable {
    private R result;

    @Override
    public void run() {
        onSuccessResponse(getResult());
    }

    public abstract void onSuccessResponse(R results);
}
