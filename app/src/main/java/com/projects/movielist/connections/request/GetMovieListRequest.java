package com.projects.movielist.connections.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder

public class GetMovieListRequest {

    private int page;
    private String apiKey;
}
