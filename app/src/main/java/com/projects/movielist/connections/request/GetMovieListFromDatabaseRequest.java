package com.projects.movielist.connections.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter

public class GetMovieListFromDatabaseRequest {

    private int pageNo;
    private int pageSize;
}
