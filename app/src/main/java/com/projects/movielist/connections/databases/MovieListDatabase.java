package com.projects.movielist.connections.databases;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.projects.movielist.GlobalApp;
import com.projects.movielist.connections.request.GetMovieListFromDatabaseRequest;
import com.projects.movielist.connections.request.GetMovieListRequest;
import com.projects.movielist.connections.response.DatabaseResponseErrorListener;
import com.projects.movielist.connections.response.DatabaseResponseSuccessListener;
import com.projects.movielist.models.MovieModel;

import java.util.List;

public class MovieListDatabase implements InterfaceMovieListDatabase{

    private @Nullable
    Activity currentActivity;
    private final MovieListDatabaseDefinition databaseDefinition;

    public MovieListDatabase (@Nullable Context context) {
        if (context != null) {
            if (context instanceof Activity) {
                this.currentActivity = (Activity) context;
            }
        }

        this.databaseDefinition = GlobalApp.getNewInstance().getDatabaseDefinition();
    }

    @Override
    public void insertStore(MovieModel movieModel, DatabaseResponseSuccessListener<Long> successListener, DatabaseResponseErrorListener errorListener) {
        Log.v("responseCek", movieModel.toString());
        new Thread(){
            @Override
            public void run() {
                Long id = databaseDefinition.movieDao().insert(movieModel);
                successListener.setResult(id);

                if (currentActivity != null){
                    currentActivity.runOnUiThread(successListener);
                    return;
                }
                successListener.run();
            }
        }.start();
    }

    @Override
    public void getMovieList(GetMovieListFromDatabaseRequest request, DatabaseResponseSuccessListener<List<MovieModel>> successListener, DatabaseResponseErrorListener errorListener) {
        new Thread(){
            @Override
            public void run() {
                List<MovieModel> movieModels = databaseDefinition.movieDao()
                        .getMovieList(request.getPageNo(), request.getPageSize());

                successListener.setResult(movieModels);

                if (currentActivity != null){
                    currentActivity.runOnUiThread(successListener);
                    return;
                }
                successListener.run();
            }
        }.start();
    }

    @Override
    public void getTotalMovie(Integer pageSize, DatabaseResponseSuccessListener<Integer> successListener, DatabaseResponseErrorListener errorListener) {
        new Thread(){
            @Override
            public void run() {
                Integer totalMovie = databaseDefinition.movieDao()
                        .totalMovie();

                Double totalPage = Math.ceil(totalMovie/Double.valueOf(pageSize));

                successListener.setResult(totalPage.intValue());

                if (currentActivity != null){
                    currentActivity.runOnUiThread(successListener);
                    return;
                }
                successListener.run();
            }
        }.start();
    }

}
