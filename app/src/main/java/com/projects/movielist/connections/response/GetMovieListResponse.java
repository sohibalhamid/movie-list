package com.projects.movielist.connections.response;

import com.projects.movielist.models.MovieModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter

public class GetMovieListResponse {

    private int totalPage = 0;
    private List<MovieModel> movieModelList = new ArrayList<>();

    public GetMovieListResponse(JSONObject response){
        try {
            if (response.has("results")){
                JSONArray jsonArray = response.getJSONArray("results");
                for (int index = 0; index < jsonArray.length(); index++){
                    JSONObject jsonObject = jsonArray.getJSONObject(index);
                    MovieModel movieModel = new MovieModel(jsonObject);
                    movieModelList.add(movieModel);
                }
            }
            if (response.has("total_pages")){
                totalPage = response.getInt("total_pages");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
