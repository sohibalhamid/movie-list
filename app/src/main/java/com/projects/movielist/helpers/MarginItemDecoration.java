package com.projects.movielist.helpers;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MarginItemDecoration extends RecyclerView.ItemDecoration {

    private final int itemSpace;
    private final String type;

    public MarginItemDecoration(float itemSpace, String type){
        this.itemSpace = (int) itemSpace;
        this.type = type;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (type == null)
            outRect.set(itemSpace, itemSpace, itemSpace, itemSpace);
        else{
            if (type.equalsIgnoreCase(GlobalVariable.KEY_ORIENTASI_VERTICAL)){
                if (parent.getChildAdapterPosition(view) == 0){
                    outRect.set(0, itemSpace, 0, itemSpace);
                } else {
                    outRect.set(0, 0, 0, itemSpace);
                }
            }
            if (type.equalsIgnoreCase(GlobalVariable.KEY_ORIENTASI_HORIZONTAL)){
                if (parent.getChildAdapterPosition(view) == 0){
                    outRect.set(itemSpace, 0, itemSpace, 0);
                } else {
                    outRect.set(0, 0, itemSpace, 0);
                }
            }
        }
    }

    @androidx.databinding.BindingAdapter("setupHorizontalRecyclerView")
    public static void setupHorizontalRecyclerView(RecyclerView recyclerView, float space){
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.HORIZONTAL, false));
        recyclerView.addItemDecoration(new MarginItemDecoration(space, GlobalVariable.KEY_ORIENTASI_HORIZONTAL)
        );
    }
}
