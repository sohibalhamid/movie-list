package com.projects.movielist.models;

import android.app.Application;

import com.projects.movielist.GlobalApp;

public class MovieListAplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        GlobalApp.getNewInstance().initDatabase(getApplicationContext());
    }
}
