package com.projects.movielist.connections.databases;

import com.projects.movielist.connections.request.GetMovieListFromDatabaseRequest;
import com.projects.movielist.connections.request.GetMovieListRequest;
import com.projects.movielist.connections.response.DatabaseResponseErrorListener;
import com.projects.movielist.connections.response.DatabaseResponseSuccessListener;
import com.projects.movielist.models.MovieModel;

import java.util.List;

public interface InterfaceMovieListDatabase {

    void insertStore(MovieModel movieModel,
                     DatabaseResponseSuccessListener<Long> successListener,
                     DatabaseResponseErrorListener errorListener);

    void getMovieList(GetMovieListFromDatabaseRequest request,
                      DatabaseResponseSuccessListener<List<MovieModel>> successListener,
                      DatabaseResponseErrorListener errorListener);

    void getTotalMovie(Integer pageSize,
                       DatabaseResponseSuccessListener<Integer> successListener,
                       DatabaseResponseErrorListener errorListener);
}
