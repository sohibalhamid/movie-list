package com.projects.movielist;

import android.content.Context;

import androidx.room.Room;

import com.projects.movielist.connections.databases.InterfaceMovieListDatabase;
import com.projects.movielist.connections.databases.MovieListDatabase;
import com.projects.movielist.connections.databases.MovieListDatabaseDefinition;

public class GlobalApp {
    private static final GlobalApp newInstance = new GlobalApp();

    private MovieListDatabaseDefinition databaseDefinition;

    public static GlobalApp getNewInstance() {
        return newInstance;
    }

    private GlobalApp(){}

    public InterfaceMovieListDatabase getMovieListDatabase(Context context){
        return new MovieListDatabase(context);
    }

    public void initDatabase(Context context){
        databaseDefinition = Room.databaseBuilder(context, MovieListDatabaseDefinition.class, context.getString(R.string.app_name)).build();
    }

    public MovieListDatabaseDefinition getDatabaseDefinition(){
        return databaseDefinition;
    }

}
