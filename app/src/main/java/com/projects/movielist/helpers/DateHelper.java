package com.projects.movielist.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
    private final SimpleDateFormat dateFormat;
    private final SimpleDateFormat apiDateFormat;

    private DateHelper(){
        dateFormat = new SimpleDateFormat("MMM dd yyyy", new Locale("en", "EN"));
        apiDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    public static DateHelper newInstance() {
        DateHelper dateHelper = new DateHelper();
        return dateHelper;
    }

    public String convertApiDateToDateTime(String inputDate){
        Date date;
        String outputDate = "";

        if (inputDate != null) {
            try {
                date = apiDateFormat.parse(inputDate);
                if (date != null) outputDate = dateFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return outputDate;
    }
}
