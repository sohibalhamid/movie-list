package com.projects.movielist.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import org.json.JSONObject;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(indices = @Index(value = "id", unique = true))

public class MovieModel {

    @PrimaryKey()
    @SerializedName("id")
    @ColumnInfo(name = "id")
    private int id;

    @SerializedName("title")
    @ColumnInfo(name = "title")
    private String title;

    @SerializedName("poster_path")
    @ColumnInfo(name = "poster_path")
    private String posterPath;

    @SerializedName("release_date")
    @ColumnInfo(name = "release_date")
    private String releaseDate;

    @SerializedName("vote_average")
    @ColumnInfo(name = "vote_average")
    private Double voteAverage;

    public MovieModel(JSONObject jsonObject){
        try {
            if (jsonObject.has("id")){
                setId(jsonObject.getInt("id"));
            }
            if (jsonObject.has("title")){
                setTitle(jsonObject.getString("title"));
            }
            if (jsonObject.has("poster_path")){
                setPosterPath(jsonObject.getString("poster_path"));
            }
            if (jsonObject.has("release_date")){
                setReleaseDate(jsonObject.getString("release_date"));
            }
            if (jsonObject.has("vote_average")){
                setVoteAverage(jsonObject.getDouble("vote_average"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
