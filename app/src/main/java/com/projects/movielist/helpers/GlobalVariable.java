package com.projects.movielist.helpers;

public class GlobalVariable {

    public static final String POSTER_FILE_SIZE = "w300/";
    public static final String KEY_ORIENTASI_VERTICAL = "vertical";
    public static final String KEY_ORIENTASI_HORIZONTAL = "horizontal";

    public static final String KEY_URL_PAGE = "page";
    public static final String KEY_URL_API_KEY = "api_key";

    public static final String CODE_PREV = "prev";
    public static final String CODE_NEXT = "next";
    public static final Integer PAGE_SIZE = 10;

    public static final String CONFIG_API_KEY = "22998b09c4437266018e383590292824";

}
