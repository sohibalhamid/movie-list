package com.projects.movielist.features.movielist;

import android.view.View;

import androidx.lifecycle.ViewModelProviders;

import com.projects.movielist.R;
import com.projects.movielist.adapters.MovieAdapter;
import com.projects.movielist.connections.request.GetMovieListFromDatabaseRequest;
import com.projects.movielist.connections.request.GetMovieListRequest;
import com.projects.movielist.databinding.ActivityMovieListBinding;
import com.projects.movielist.features.BaseActivity;
import com.projects.movielist.features.bottomsheets.BottomSheetNotification;
import com.projects.movielist.helpers.ConnectionDetectorHelper;
import com.projects.movielist.helpers.GlobalVariable;

import java.util.ArrayList;

public class MovieListActivity extends BaseActivity<ActivityMovieListBinding> {

    private MovieListViewModel movieListViewModel;
    private MovieAdapter movieAdapter;
    private int countPage;
    private int maxPage;
    private boolean isError;

    @Override
    protected int layout() {
        return R.layout.activity_movie_list;
    }

    @Override
    protected void initData() {
        super.initData();
        countPage = 1;
        movieAdapter = new MovieAdapter(new ArrayList<>(), this);

        getMovieList();
    }

    @Override
    protected void initView() {
        super.initView();
        binding.setMovieAdapter(movieAdapter);

        binding.tvPrev.setOnClickListener(onClick -> paginationClicked("prev"));
        binding.tvNext.setOnClickListener(onCLick -> paginationClicked("next"));

        if (countPage == 1) binding.tvPrev.setVisibility(View.INVISIBLE);
        else binding.tvPrev.setVisibility(View.VISIBLE);
    }

    private void getMovieList(){
        binding.progressBar.setVisibility(View.VISIBLE);
        if (ConnectionDetectorHelper.newInstance().checkConnection(this)){
            getMovieListFromAPI();
        } else {
            isError = true;
            getMovieListFromDatabase();
        }
    }

    private void getMovieListFromAPI(){
        GetMovieListRequest request = GetMovieListRequest.builder()
                .page(countPage)
                .apiKey(GlobalVariable.CONFIG_API_KEY)
                .build();

        movieListViewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);
        movieListViewModel.getMovieList(request);
        movieListViewModel.getTotalPageLiveData().observe(this, response -> {
            maxPage = response;
        });
        movieListViewModel.getMovieLiveData().observe(this, response -> {
            if (response.isEmpty()){
                binding.tvNext.setVisibility(View.GONE);
                binding.tvPrev.setVisibility(View.GONE);
                if (isError) popupNotification(getString(R.string.message_error));
                else popupNotification(getString(R.string.message_empty));
            } else {
                movieListViewModel.insertMovieListToDatabase(response);
                movieAdapter.setMainData(response);
            }
            binding.progressBar.setVisibility(View.GONE);
            binding.tvPrev.setEnabled(true);
            binding.tvNext.setEnabled(true);
        });
    }

    private void getMovieListFromDatabase(){
        GetMovieListFromDatabaseRequest request = GetMovieListFromDatabaseRequest.builder()
                .pageNo(countPage)
                .pageSize(10)
                .build();

        movieListViewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);
        movieListViewModel.getMovieListFromDatabase(request);
        movieListViewModel.getTotalPageDatabaseLiveData().observe(this, response ->{
            maxPage = response;
        });
        movieListViewModel.getMovieLiveData().observe(this, response ->{
            binding.progressBar.setVisibility(View.GONE);
            binding.tvPrev.setEnabled(true);
            binding.tvNext.setEnabled(true);
            if (response.isEmpty()){
                binding.tvNext.setVisibility(View.GONE);
                binding.tvPrev.setVisibility(View.GONE);
                if (isError) popupNotification(getString(R.string.message_error));
                else popupNotification(getString(R.string.message_empty));
            } else {
                movieAdapter.setMainData(response);
            }
        });
    }

    private void paginationClicked(String code){
        if (code.equals(GlobalVariable.CODE_PREV)) countPage--;
        if (code.equals(GlobalVariable.CODE_NEXT)) countPage++;

        if (countPage == 1) binding.tvPrev.setVisibility(View.INVISIBLE);
        else binding.tvPrev.setVisibility(View.VISIBLE);

        if (countPage == maxPage) binding.tvNext.setVisibility(View.INVISIBLE);
        else binding.tvNext.setVisibility(View.VISIBLE);

        binding.tvPrev.setEnabled(false);
        binding.tvNext.setEnabled(false);

        getMovieList();
    }

    private void popupNotification(String description){
        new BottomSheetNotification.Builder()
                .setDescription(description)
                .build(this)
                .show();
    }
}