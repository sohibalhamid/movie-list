package com.projects.movielist.connections.databases;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.projects.movielist.models.MovieModel;

import java.util.List;

@Dao

public interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert (MovieModel movieModel);

    @Query("SELECT * FROM MovieModel ORDER BY id ASC LIMIT :pageNo, :pageSize")
    List<MovieModel> getMovieList (Integer pageNo, Integer pageSize);

    @Query("SELECT COUNT(*) FROM MovieModel")
    Integer totalMovie();
}
