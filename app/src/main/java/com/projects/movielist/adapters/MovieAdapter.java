package com.projects.movielist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.projects.movielist.BuildConfig;
import com.projects.movielist.R;
import com.projects.movielist.databinding.ItemMovieBinding;
import com.projects.movielist.helpers.GlobalVariable;
import com.projects.movielist.models.MovieModel;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {
    private List<MovieModel> movieModelList;
    private final Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemMovieBinding binding;

        public MyViewHolder(@NonNull ItemMovieBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public MovieAdapter(List<MovieModel> movieModels, Context context){
        this.movieModelList = movieModels;
        this.context = context;
    }

    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemMovieBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_movie, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MyViewHolder holder, int position) {
        final MovieModel movieModel = movieModelList.get(position);
        holder.binding.setMovieModel(movieModel);
        Glide.with(context).asBitmap().load(BuildConfig.BASE_URL_IMAGE + GlobalVariable.POSTER_FILE_SIZE + movieModel.getPosterPath()).into(holder.binding.imgPoster);
    }

    @Override
    public int getItemCount() {
        return movieModelList.size();
    }

    public void setMainData(List<MovieModel> mainData) {
        this.movieModelList = mainData;
        notifyDataSetChanged();
    }
}
