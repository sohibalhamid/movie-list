package com.projects.movielist.helpers;

public class TextHelper {

    private TextHelper(){ }

    public static TextHelper newInstance(){
        TextHelper textHelper = new TextHelper();
        return textHelper;
    }

    public String convertToPercentString(Double originalValue){
        if (originalValue != null && originalValue != 0){
            Double result = (originalValue/10) * 100;
            Integer intResult = result.intValue();
            return String.valueOf(intResult);
        }
        return "0";
    }

    public Integer convertToPercentInt(Double originalValue){
        if (originalValue != null && originalValue != 0){
            Double result = (originalValue/10) * 100;
            return result.intValue();
        }
        return 0;
    }
}
