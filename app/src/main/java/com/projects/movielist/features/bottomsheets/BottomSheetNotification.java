package com.projects.movielist.features.bottomsheets;

import android.content.Context;
import android.view.LayoutInflater;

import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.projects.movielist.R;
import com.projects.movielist.databinding.BottomSheetNotificationBinding;

public class BottomSheetNotification {
    private BottomSheetDialog dialog;
    private final Context context;
    private final String description;

    public BottomSheetNotification(Context context, Builder builder) {
        this.context = context;
        this.description = builder.description;

        bind();
    }

    public interface BottomSheetEditTextListener {
        void onClick();
    }

    public static class Builder {
        private String description;

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public BottomSheetNotification build (Context context) {
            return new BottomSheetNotification(context, this);
        }
    }

    private void bind() {
        BottomSheetNotificationBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(this.context),
                R.layout.bottom_sheet_notification,
                null,
                false
        );

        binding.btnOk.setOnClickListener(onClick -> dismiss());
        binding.tvDesc.setText(description);
        dialog = new BottomSheetDialog(context, R.style.BottomAlertTheme);
        dialog.setContentView(binding.getRoot());
        dialog.setCanceledOnTouchOutside(false);
    }

    public void show() {
        if (dialog != null) dialog.show();
    }

    public void dismiss() {
        if (dialog != null) dialog.dismiss();
    }
}