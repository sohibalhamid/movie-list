package com.projects.movielist.connections.networks;

import com.projects.movielist.BuildConfig;

public interface PathUrl {

    static String getListId1(){
        return BuildConfig.BASE_URL + "list/1";
    }

}
