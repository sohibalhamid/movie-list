package com.projects.movielist.connections.databases;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.projects.movielist.BuildConfig;
import com.projects.movielist.models.MovieModel;

@Database(
        entities = {MovieModel.class}, version = BuildConfig.DATABASE_VERSION_CODE, exportSchema = false
)

public abstract class MovieListDatabaseDefinition extends RoomDatabase {
    public abstract MovieDao movieDao();
}
