package com.projects.movielist.features;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity <Binding extends ViewDataBinding> extends AppCompatActivity {
    abstract protected int layout();
    protected Binding binding;

    protected void initData(){

    }

    protected void initView(){

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,layout());
        initData();
        initView();
    }
}
